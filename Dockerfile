FROM alpine:3.17
RUN apk add cmake git ninja gcc-riscv-none-elf binutils-riscv64 musl-dev
COPY . /mnt
WORKDIR /mnt/
RUN sh ./build.sh
